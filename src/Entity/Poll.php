<?php

namespace App\Entity;

use App\Repository\PollRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PollRepository::class)]
class Poll
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $title = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $description = null;

    #[ORM\Column(length: 255)]
    private ?string $creator_name = null;

    #[ORM\Column(length: 255)]
    private ?string $creator_mail = null;

    #[ORM\Column(length: 255)]
    private ?string $public_slug = null;

    #[ORM\OneToMany(mappedBy: 'poll', targetEntity: Slot::class, orphanRemoval: true)]
    private Collection $slots;

    #[ORM\OneToMany(mappedBy: 'poll', targetEntity: Ballot::class, orphanRemoval: true)]
    private Collection $ballots;

    public function __construct()
    {
        $this->slots = new ArrayCollection();
        $this->ballots = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCreatorName(): ?string
    {
        return $this->creator_name;
    }

    public function setCreatorName(string $creator_name): self
    {
        $this->creator_name = $creator_name;

        return $this;
    }

    public function getCreatorMail(): ?string
    {
        return $this->creator_mail;
    }

    public function setCreatorMail(string $creator_mail): self
    {
        $this->creator_mail = $creator_mail;

        return $this;
    }

    public function getPublicSlug(): ?string
    {
        return $this->public_slug;
    }

    public function setPublicSlug(string $public_slug): self
    {
        $this->public_slug = $public_slug;

        return $this;
    }

    /**
     * @return Collection<int, Slot>
     */
    public function getSlots(): Collection
    {
        return $this->slots;
    }

    public function addSlot(Slot $slot): self
    {
        if (!$this->slots->contains($slot)) {
            $this->slots->add($slot);
            $slot->setPoll($this);
        }

        return $this;
    }

    public function removeSlot(Slot $slot): self
    {
        if ($this->slots->removeElement($slot)) {
            // set the owning side to null (unless already changed)
            if ($slot->getPoll() === $this) {
                $slot->setPoll(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Ballot>
     */
    public function getBallots(): Collection
    {
        return $this->ballots;
    }

    public function addBallot(Ballot $ballot): self
    {
        if (!$this->ballots->contains($ballot)) {
            $this->ballots->add($ballot);
            $ballot->setPoll($this);
        }

        return $this;
    }

    public function removeBallot(Ballot $ballot): self
    {
        if ($this->ballots->removeElement($ballot)) {
            // set the owning side to null (unless already changed)
            if ($ballot->getPoll() === $this) {
                $ballot->setPoll(null);
            }
        }

        return $this;
    }
}
