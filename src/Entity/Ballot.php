<?php

namespace App\Entity;

use App\Repository\BallotRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: BallotRepository::class)]
class Ballot
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'ballots')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Poll $poll = null;

    #[ORM\Column(length: 255)]
    private ?string $voter = null;

    #[ORM\Column(length: 255)]
    private ?string $choices = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPoll(): ?Poll
    {
        return $this->poll;
    }

    public function setPoll(?Poll $poll): self
    {
        $this->poll = $poll;

        return $this;
    }

    public function getVoter(): ?string
    {
        return $this->voter;
    }

    public function setVoter(string $voter): self
    {
        $this->voter = $voter;

        return $this;
    }

    public function getChoices(): ?string
    {
        return $this->choices;
    }

    public function setChoices(string $choices): self
    {
        $this->choices = $choices;

        return $this;
    }
}
